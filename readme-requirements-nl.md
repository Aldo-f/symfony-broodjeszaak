# Extra opdracht PHP MVC – Broodjes

<p>
In de kantine van het opleidingscentrum kunnen vòòr 10u broodjes besteld worden. Men heeft besloten om een webtoepassing te maken waarmee cursisten dit per computer kunnen doen. Elke cursist moet zich eerst registreren bij het systeem. De aanmelding achteraf zal gebeuren met emailadres en wachtwoord. Dit wachtwoord mag de cursist evenwel niet zelf kiezen. Het wordt gegenereerd (4 tekens, cijfers, grote- en kleine letters mogelijk), en per e-mail verstuurd nadat de registratie voltooid is. De cursist moet dus na registratie eerst zijn mailbox checken om het wachtwoord op te halen en aan te melden. Voorzie een optie waarmee de cursist zichzelf een nieuw wachtwoord kan laten mailen wanneer hij het huidige vergeten is. </p><p>
Na aanmelding krijgt hij/zij een lijst van types broodjes (klein grof, groot grof, klein wit, groot wit, ciabatta,…) te zien waaruit een keuze kan gemaakt worden. Dit broodje kan vervolgens worden belegd (meerdere soorten beleg per broodje zijn mogelijk): hesp, kaas, tomaat, sla, boter mayonaise,… Er is een vastgelegde prijs per type beleg en type broodje. </p><p>
Omdat de kantine deze bestelling tijdig door dient te geven, is bestellen tot ten laatste 9u59 mogelijk. Vanaf 10u00 kan je dan ook enkel nog zien wat je besteld hebt die dag (slechts één bestelling per dag is mogelijk, maar uiteraard kunnen de hongerigen meerdere broodjes in één bestelling opnemen). </p>

## Walktrough

### Wat werd gevraagd?

- [ ] Registratie en aanmelding
  - [ ] Registratie via mail
  - [ ] Wachtwoord krijg je via mail
  - [ ] 4 tekens (cijfers, grote/kleine letters)
  - [ ] Nieuw wachtwoord indien vergeten
- [ ] Broodjes
  - [x] Type: klein, groot, grof, ciabatta, donker, wit, ...
  - [x] Belegd: hesp, kaas, tomaat, sla, boter, mayonaise, ...
  - [ ] Prijs: per type (combinaties) en per beleg (combinaties)
  - [ ] Samenvatting van wat je bestelde
- [ ] Bestelling voor 10u
- [ ] Vanaf 9u59 enkel te zien wat je bestelde
- [ ] 1 bestelling per dag (met eventueel meerdere broodjes)
