-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: symfony_broodjes
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allergy`
--

DROP TABLE IF EXISTS `allergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `allergy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergy`
--

LOCK TABLES `allergy` WRITE;
/*!40000 ALTER TABLE `allergy` DISABLE KEYS */;
INSERT INTO `allergy` VALUES (1,'Lactose','Vooral terug te vinden bij kaas, en de meest voorkomende voedselallergie');
/*!40000 ALTER TABLE `allergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20191205164022','2019-12-05 16:40:39'),('20191206071826','2019-12-06 07:18:55'),('20191209142741','2019-12-09 14:27:51'),('20191209152347','2019-12-09 15:24:20'),('20191210103137','2019-12-10 10:32:40'),('20200103112143','2020-01-03 11:22:22'),('20200103140243','2020-01-03 14:02:53');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `order_status_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F5299398A76ED395` (`user_id`),
  KEY `IDX_F5299398D7707B45` (`order_status_id`),
  CONSTRAINT `FK_F5299398A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_F5299398D7707B45` FOREIGN KEY (`order_status_id`) REFERENCES `order_status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` VALUES (74,5,4,'2019-12-17 17:10:20',0),(75,5,4,'2019-12-18 15:08:21',0),(76,5,4,'2019-12-18 15:58:50',0),(77,5,4,'2019-12-18 16:18:48',0),(79,5,4,'2019-12-20 08:30:26',0),(80,5,4,'2019-12-20 08:37:51',0),(81,5,4,'2020-01-03 15:13:21',680),(82,5,4,'2020-01-03 15:18:45',40);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_line`
--

DROP TABLE IF EXISTS `order_line`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_line` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sandwich_id` int(11) NOT NULL,
  `order__id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `shopping_cart_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9CE58EE14D566043` (`sandwich_id`),
  KEY `IDX_9CE58EE1251A8A50` (`order__id`),
  KEY `IDX_9CE58EE145F80CD` (`shopping_cart_id`),
  CONSTRAINT `FK_9CE58EE1251A8A50` FOREIGN KEY (`order__id`) REFERENCES `order` (`id`),
  CONSTRAINT `FK_9CE58EE145F80CD` FOREIGN KEY (`shopping_cart_id`) REFERENCES `shopping_cart` (`id`),
  CONSTRAINT `FK_9CE58EE14D566043` FOREIGN KEY (`sandwich_id`) REFERENCES `sandwich` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_line`
--

LOCK TABLES `order_line` WRITE;
/*!40000 ALTER TABLE `order_line` DISABLE KEYS */;
INSERT INTO `order_line` VALUES (87,1,74,12,NULL),(90,24,75,1,NULL),(91,2,75,1,NULL),(94,2,76,2,NULL),(95,1,77,1,NULL),(100,1,79,1,NULL),(101,2,80,1,NULL),(103,1,81,2,NULL),(104,26,NULL,1,45),(105,2,NULL,3,45),(106,1,NULL,7,45),(107,2,81,1,NULL),(108,25,81,2,NULL),(109,27,81,1,NULL),(110,1,82,1,NULL),(111,1,NULL,1,47);
/*!40000 ALTER TABLE `order_line` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_status`
--

DROP TABLE IF EXISTS `order_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_status`
--

LOCK TABLES `order_status` WRITE;
/*!40000 ALTER TABLE `order_status` DISABLE KEYS */;
INSERT INTO `order_status` VALUES (1,'Basket'),(2,'Wish'),(3,'Waiting for payment'),(4,'In progress'),(5,'On hold'),(6,'Completed'),(7,'Cancelled'),(8,'Reimbursed');
/*!40000 ALTER TABLE `order_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sandwich`
--

DROP TABLE IF EXISTS `sandwich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sandwich` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_bread_id` int(11) NOT NULL,
  `type_size_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `a_la_carte` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8827086890CB2233` (`type_bread_id`),
  KEY `IDX_8827086861ACC7D2` (`type_size_id`),
  CONSTRAINT `FK_8827086861ACC7D2` FOREIGN KEY (`type_size_id`) REFERENCES `type_size` (`id`),
  CONSTRAINT `FK_8827086890CB2233` FOREIGN KEY (`type_bread_id`) REFERENCES `type_bread` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sandwich`
--

LOCK TABLES `sandwich` WRITE;
/*!40000 ALTER TABLE `sandwich` DISABLE KEYS */;
INSERT INTO `sandwich` VALUES (1,1,3,'Kaas','Broodje met overheerlijke kaas',1),(2,1,3,'Smos','Broodje smos',1),(24,2,3,'Custom sandwich','Custom sandwich',0),(25,2,6,'Custom sandwich','Custom sandwich',0),(26,1,1,'Custom sandwich','Custom sandwich',0),(27,2,1,'Custom sandwich','Custom sandwich',0);
/*!40000 ALTER TABLE `sandwich` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sandwich_type_spread`
--

DROP TABLE IF EXISTS `sandwich_type_spread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sandwich_type_spread` (
  `sandwich_id` int(11) NOT NULL,
  `type_spread_id` int(11) NOT NULL,
  PRIMARY KEY (`sandwich_id`,`type_spread_id`),
  KEY `IDX_E54E38F34D566043` (`sandwich_id`),
  KEY `IDX_E54E38F39D69465E` (`type_spread_id`),
  CONSTRAINT `FK_E54E38F34D566043` FOREIGN KEY (`sandwich_id`) REFERENCES `sandwich` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E54E38F39D69465E` FOREIGN KEY (`type_spread_id`) REFERENCES `type_spread` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sandwich_type_spread`
--

LOCK TABLES `sandwich_type_spread` WRITE;
/*!40000 ALTER TABLE `sandwich_type_spread` DISABLE KEYS */;
INSERT INTO `sandwich_type_spread` VALUES (1,1),(2,1),(2,2),(24,1),(24,2),(25,3),(26,1),(27,3);
/*!40000 ALTER TABLE `sandwich_type_spread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shopping_cart`
--

DROP TABLE IF EXISTS `shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shopping_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_72AAD4F6A76ED395` (`user_id`),
  CONSTRAINT `FK_72AAD4F6A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shopping_cart`
--

LOCK TABLES `shopping_cart` WRITE;
/*!40000 ALTER TABLE `shopping_cart` DISABLE KEYS */;
INSERT INTO `shopping_cart` VALUES (45,14,'2019-12-20 19:26:38'),(47,5,'2020-01-03 16:17:30');
/*!40000 ALTER TABLE `shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_bread`
--

DROP TABLE IF EXISTS `type_bread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_bread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `calories` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_bread`
--

LOCK TABLES `type_bread` WRITE;
/*!40000 ALTER TABLE `type_bread` DISABLE KEYS */;
INSERT INTO `type_bread` VALUES (1,'Wit',20,NULL),(2,'Bruin',60,NULL);
/*!40000 ALTER TABLE `type_bread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_bread_allergy`
--

DROP TABLE IF EXISTS `type_bread_allergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_bread_allergy` (
  `type_bread_id` int(11) NOT NULL,
  `allergy_id` int(11) NOT NULL,
  PRIMARY KEY (`type_bread_id`,`allergy_id`),
  KEY `IDX_1DB578F490CB2233` (`type_bread_id`),
  KEY `IDX_1DB578F4DBFD579D` (`allergy_id`),
  CONSTRAINT `FK_1DB578F490CB2233` FOREIGN KEY (`type_bread_id`) REFERENCES `type_bread` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1DB578F4DBFD579D` FOREIGN KEY (`allergy_id`) REFERENCES `allergy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_bread_allergy`
--

LOCK TABLES `type_bread_allergy` WRITE;
/*!40000 ALTER TABLE `type_bread_allergy` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_bread_allergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_size`
--

DROP TABLE IF EXISTS `type_size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_size` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `multiplier` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_size`
--

LOCK TABLES `type_size` WRITE;
/*!40000 ALTER TABLE `type_size` DISABLE KEYS */;
INSERT INTO `type_size` VALUES (1,'XS',50),(2,'S',75),(3,'M',100),(4,'L',125),(5,'XL',150),(6,'XXL',200);
/*!40000 ALTER TABLE `type_size` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_spread`
--

DROP TABLE IF EXISTS `type_spread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_spread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `calories` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_spread`
--

LOCK TABLES `type_spread` WRITE;
/*!40000 ALTER TABLE `type_spread` DISABLE KEYS */;
INSERT INTO `type_spread` VALUES (1,'Kaas',20,20),(2,'Hesp',20,NULL),(3,'Tonijn',60,NULL);
/*!40000 ALTER TABLE `type_spread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_spread_allergy`
--

DROP TABLE IF EXISTS `type_spread_allergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_spread_allergy` (
  `type_spread_id` int(11) NOT NULL,
  `allergy_id` int(11) NOT NULL,
  PRIMARY KEY (`type_spread_id`,`allergy_id`),
  KEY `IDX_A7015C1F9D69465E` (`type_spread_id`),
  KEY `IDX_A7015C1FDBFD579D` (`allergy_id`),
  CONSTRAINT `FK_A7015C1F9D69465E` FOREIGN KEY (`type_spread_id`) REFERENCES `type_spread` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A7015C1FDBFD579D` FOREIGN KEY (`allergy_id`) REFERENCES `allergy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_spread_allergy`
--

LOCK TABLES `type_spread_allergy` WRITE;
/*!40000 ALTER TABLE `type_spread_allergy` DISABLE KEYS */;
INSERT INTO `type_spread_allergy` VALUES (1,1);
/*!40000 ALTER TABLE `type_spread_allergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'klant-1@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$OWZ3R1ZOdHRrcDJtakxyNw$LEsFS1HG1iW4mWxA6Eve/2clRcLuB6wDWoCJCmt3VeM'),(2,'klant-2@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$TEdLYm9wS0R0eXFpeW5COA$j48YK4YUKV8r6GwsNgKojMi7lvDePU/Twp9btx2SX0E'),(3,'klant-3@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$U05hUzl3amk1RnY0dDQxNQ$VXNSd7GUW72b2cjOQfuHChNFLrrynxFk5HLQD5UaoN0'),(4,'klant-4@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$SGxNd2YwNm9Xb3Frbi8zRg$OmC6JJEJhTh3CkYYOmUQ4mSaQVC8f4XF8eBlJDttGGI'),(5,'klant-5@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$N0RPSk8yVU5MdFIxRGhZVw$lyejlFp+P6r22+yFYKwShYVvYs8oo52ht01ogBZUn0M'),(6,'klant-6@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$ZlVYWmduLlJFWUVpbDNxSg$mILIw+s3zc9F8wH813QSj3b8+RvI0h1erJ0+ElYPoiI'),(7,'dsfgh,hgfdx','[]','$argon2id$v=19$m=65536,t=4,p=1$TVlROWxtcW1DRWRiNEhxdg$Qc8zx4ciQxVwDJP+ALpo1b7Aa96VtrqSsW0f45wDirE'),(8,'klant-7@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$aDR1MlRaNmI1ZDg4VlEvbg$JsK5Ct+rdtiehIC+yrAgN0SJqxAq4iarOU0Z0FYnZ4A'),(9,'klant-9@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$allrS3VNZGt2dklYVnQ3eA$s4/jahytRvuMXe1QUFI8tuUlpgxpqjKDu8nGiyhutnE'),(10,'klant-10@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$ZFRub0Jjc2kwV0dzLkpMWQ$mDhJ2KcmYRefXgsliV/mISXY178ZuIZt7RA5qpaApb4'),(11,'klant-11@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$UlV1Ti9JdVZDaDdUQTFwcQ$9DyGCf+KoV5rV7TiF+0pTozqMHayKNzLFK471i01UQA'),(12,'klant-12@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$N25PTzBFM1pYZzNqckZBOQ$oAruZC7A1LeNrxFxY6l63X8O+3zRLelqxNUvekM1iKQ'),(13,'klant-14@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$MEhKb0taeVBQQjZnL3B0OQ$Lox6Zcn/ftvo5Jj0obXTZRFTKT+ACakgIVto7WDC/6Y'),(14,'klant@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$WExmNU9qdi5OTkhwRzA0Ug$7rJ90SGSJP2h9K+ExEQXaTN6lUxTTpkXFQfrAF/5ZSg'),(15,'aldo@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$NTA3MDhEUlRCQ3h1T0ZtYQ$bDWTl+ktSNBke8Z5DdfYFQbqhkLrUlvgjMWhhxAjeFk'),(16,'dosykyq@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$S2JPYklxRm8vS0tpeC5EOA$nL62hu1w5ZzjTCdOfq8QPbmW1iX8CrBYqhZJ2/62wo0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-01-03 16:42:45
