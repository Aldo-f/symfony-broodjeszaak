SELECT 
    *, COUNT(sts.type_spread_id) cnt
FROM
    symfony_broodjes.sandwich s
        INNER JOIN
    sandwich_type_spread sts ON s.id = sts.sandwich_id
WHERE
    s.type_bread_id = 1 
    AND s.type_size_id = 3
	AND sts.type_spread_id IN  ( 1 , 2 )
GROUP BY s.id
HAVING cnt = 2

