CREATE DATABASE  IF NOT EXISTS `symfony_broodjes` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `symfony_broodjes`;
-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: symfony_broodjes
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `allergy`
--

DROP TABLE IF EXISTS `allergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `allergy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `allergy`
--

LOCK TABLES `allergy` WRITE;
/*!40000 ALTER TABLE `allergy` DISABLE KEYS */;
INSERT INTO `allergy` VALUES (1,'Lactose',NULL);
/*!40000 ALTER TABLE `allergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration_versions`
--

DROP TABLE IF EXISTS `migration_versions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration_versions` (
  `version` varchar(14) COLLATE utf8mb4_unicode_ci NOT NULL,
  `executed_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration_versions`
--

LOCK TABLES `migration_versions` WRITE;
/*!40000 ALTER TABLE `migration_versions` DISABLE KEYS */;
INSERT INTO `migration_versions` VALUES ('20191203155147','2019-12-03 15:52:33'),('20191203155548','2019-12-03 15:56:45'),('20191203160329','2019-12-03 16:04:27'),('20191203160949','2019-12-03 16:10:32'),('20191203161420','2019-12-03 16:14:51'),('20191203161846','2019-12-03 16:18:53'),('20191203174127','2019-12-03 17:41:43');
/*!40000 ALTER TABLE `migration_versions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order`
--

DROP TABLE IF EXISTS `order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F5299398A76ED395` (`user_id`),
  CONSTRAINT `FK_F5299398A76ED395` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order`
--

LOCK TABLES `order` WRITE;
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
/*!40000 ALTER TABLE `order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_sandwich`
--

DROP TABLE IF EXISTS `order_sandwich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `order_sandwich` (
  `order_id` int(11) NOT NULL,
  `sandwich_id` int(11) NOT NULL,
  PRIMARY KEY (`order_id`,`sandwich_id`),
  KEY `IDX_69DFEAD98D9F6D38` (`order_id`),
  KEY `IDX_69DFEAD94D566043` (`sandwich_id`),
  CONSTRAINT `FK_69DFEAD94D566043` FOREIGN KEY (`sandwich_id`) REFERENCES `sandwich` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_69DFEAD98D9F6D38` FOREIGN KEY (`order_id`) REFERENCES `order` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_sandwich`
--

LOCK TABLES `order_sandwich` WRITE;
/*!40000 ALTER TABLE `order_sandwich` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_sandwich` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sandwich`
--

DROP TABLE IF EXISTS `sandwich`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sandwich` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_bread_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8827086890CB2233` (`type_bread_id`),
  CONSTRAINT `FK_8827086890CB2233` FOREIGN KEY (`type_bread_id`) REFERENCES `type_bread` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sandwich`
--

LOCK TABLES `sandwich` WRITE;
/*!40000 ALTER TABLE `sandwich` DISABLE KEYS */;
INSERT INTO `sandwich` VALUES (1,1,'Kaas','Broodje met overheerlijke kaas');
/*!40000 ALTER TABLE `sandwich` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sandwich_type_spread`
--

DROP TABLE IF EXISTS `sandwich_type_spread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sandwich_type_spread` (
  `sandwich_id` int(11) NOT NULL,
  `type_spread_id` int(11) NOT NULL,
  PRIMARY KEY (`sandwich_id`,`type_spread_id`),
  KEY `IDX_E54E38F34D566043` (`sandwich_id`),
  KEY `IDX_E54E38F39D69465E` (`type_spread_id`),
  CONSTRAINT `FK_E54E38F34D566043` FOREIGN KEY (`sandwich_id`) REFERENCES `sandwich` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_E54E38F39D69465E` FOREIGN KEY (`type_spread_id`) REFERENCES `type_spread` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sandwich_type_spread`
--

LOCK TABLES `sandwich_type_spread` WRITE;
/*!40000 ALTER TABLE `sandwich_type_spread` DISABLE KEYS */;
INSERT INTO `sandwich_type_spread` VALUES (1,1);
/*!40000 ALTER TABLE `sandwich_type_spread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_bread`
--

DROP TABLE IF EXISTS `type_bread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_bread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `calories` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_bread`
--

LOCK TABLES `type_bread` WRITE;
/*!40000 ALTER TABLE `type_bread` DISABLE KEYS */;
INSERT INTO `type_bread` VALUES (1,'Wit',200,20);
/*!40000 ALTER TABLE `type_bread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_bread_allergy`
--

DROP TABLE IF EXISTS `type_bread_allergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_bread_allergy` (
  `type_bread_id` int(11) NOT NULL,
  `allergy_id` int(11) NOT NULL,
  PRIMARY KEY (`type_bread_id`,`allergy_id`),
  KEY `IDX_1DB578F490CB2233` (`type_bread_id`),
  KEY `IDX_1DB578F4DBFD579D` (`allergy_id`),
  CONSTRAINT `FK_1DB578F490CB2233` FOREIGN KEY (`type_bread_id`) REFERENCES `type_bread` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_1DB578F4DBFD579D` FOREIGN KEY (`allergy_id`) REFERENCES `allergy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_bread_allergy`
--

LOCK TABLES `type_bread_allergy` WRITE;
/*!40000 ALTER TABLE `type_bread_allergy` DISABLE KEYS */;
/*!40000 ALTER TABLE `type_bread_allergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_spread`
--

DROP TABLE IF EXISTS `type_spread`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_spread` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `calories` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_spread`
--

LOCK TABLES `type_spread` WRITE;
/*!40000 ALTER TABLE `type_spread` DISABLE KEYS */;
INSERT INTO `type_spread` VALUES (1,'Kaas',100,NULL);
/*!40000 ALTER TABLE `type_spread` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_spread_allergy`
--

DROP TABLE IF EXISTS `type_spread_allergy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `type_spread_allergy` (
  `type_spread_id` int(11) NOT NULL,
  `allergy_id` int(11) NOT NULL,
  PRIMARY KEY (`type_spread_id`,`allergy_id`),
  KEY `IDX_A7015C1F9D69465E` (`type_spread_id`),
  KEY `IDX_A7015C1FDBFD579D` (`allergy_id`),
  CONSTRAINT `FK_A7015C1F9D69465E` FOREIGN KEY (`type_spread_id`) REFERENCES `type_spread` (`id`) ON DELETE CASCADE,
  CONSTRAINT `FK_A7015C1FDBFD579D` FOREIGN KEY (`allergy_id`) REFERENCES `allergy` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_spread_allergy`
--

LOCK TABLES `type_spread_allergy` WRITE;
/*!40000 ALTER TABLE `type_spread_allergy` DISABLE KEYS */;
INSERT INTO `type_spread_allergy` VALUES (1,1);
/*!40000 ALTER TABLE `type_spread_allergy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'feny@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$NGw5SlJHZk1QQ3hiQU9uRA$hrXBsSKxKWFPG2yestITTVSH94lhuJRPV+6gEoGJxd4'),(2,'gyweb@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$RnFnbUE2aFZSZGlpUmM3Wg$7Kb+w38r5swxS9/UWtW+10cxdEVLlzL9VisM3efA6dc'),(3,'japyk@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$bHBlY3A1VC9KRU1sWDJ3MQ$FE2Dz6Ak042NTB0f0QFlBcFrqTSADLt4Wtv4bbaJRpU'),(4,'kubyr@getnada.com','[]','$argon2id$v=19$m=65536,t=4,p=1$b0l6LnhjVlptRm9ML0NvMA$RGJ1zPV7YaK7IO/kMSzLT7RPGEBnodq/zvo7hAl7zDg');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-03 18:42:20
