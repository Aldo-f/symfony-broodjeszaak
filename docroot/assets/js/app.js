/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
// require("../scss/app.scss");

import "../../node_modules/bootstrap/scss/bootstrap.scss";
import "../../node_modules/@fortawesome/fontawesome-free/css/all.min.css";
import "../scss/app.scss";
import "../../node_modules/bootstrap/dist/js/bootstrap.min.js";
import "../../node_modules/popper.js/dist/popper.min.js";

import $ from "jquery";

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// const $ = require("jquery");

// import the function from greet.js (the .js extension is optional)
// var greet = require("./greet");
// import greet from "./greet-import";

// $(document).ready(function() {
//   // $("body").prepend("<h1>" + greet("jill") + "</h1>");
//   console.log(greet("Dev"));
// });
