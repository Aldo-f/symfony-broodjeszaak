<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AllergyRepository")
 */
class Allergy
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TypeSpread", mappedBy="allergyId")
     */
    private $typeSpreads;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TypeBread", mappedBy="allergyId")
     */
    private $typeBreads;

    public function __construct()
    {
        $this->typeSpreads = new ArrayCollection();
        $this->typeBreads = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|TypeSpread[]
     */
    public function getTypeSpreads(): Collection
    {
        return $this->typeSpreads;
    }

    public function addTypeSpread(TypeSpread $typeSpread): self
    {
        if (!$this->typeSpreads->contains($typeSpread)) {
            $this->typeSpreads[] = $typeSpread;
            $typeSpread->addAllergyId($this);
        }

        return $this;
    }

    public function removeTypeSpread(TypeSpread $typeSpread): self
    {
        if ($this->typeSpreads->contains($typeSpread)) {
            $this->typeSpreads->removeElement($typeSpread);
            $typeSpread->removeAllergyId($this);
        }

        return $this;
    }

    /**
     * FIX
     * Catchable Fatal Error: Object of class App\Entity\Allergy could not be converted to string 
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|TypeBread[]
     */
    public function getTypeBreads(): Collection
    {
        return $this->typeBreads;
    }

    public function addTypeBread(TypeBread $typeBread): self
    {
        if (!$this->typeBreads->contains($typeBread)) {
            $this->typeBreads[] = $typeBread;
            $typeBread->addAllergyId($this);
        }

        return $this;
    }

    public function removeTypeBread(TypeBread $typeBread): self
    {
        if ($this->typeBreads->contains($typeBread)) {
            $this->typeBreads->removeElement($typeBread);
            $typeBread->removeAllergyId($this);
        }

        return $this;
    }
}