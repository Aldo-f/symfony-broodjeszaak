<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeSizeRepository")
 */
class TypeSize
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Sandwich", mappedBy="typeSize")
     */
    private $sandwiches;

    /**
     * @ORM\Column(type="integer")
     */
    private $multiplier;

    public function __construct()
    {
        $this->sandwiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Sandwich[]
     */
    public function getSandwiches(): Collection
    {
        return $this->sandwiches;
    }

    public function addSandwich(Sandwich $sandwich): self
    {
        if (!$this->sandwiches->contains($sandwich)) {
            $this->sandwiches[] = $sandwich;
            $sandwich->setTypeSize($this);
        }

        return $this;
    }

    public function removeSandwich(Sandwich $sandwich): self
    {
        if ($this->sandwiches->contains($sandwich)) {
            $this->sandwiches->removeElement($sandwich);
            // set the owning side to null (unless already changed)
            if ($sandwich->getTypeSize() === $this) {
                $sandwich->setTypeSize(null);
            }
        }

        return $this;
    }

    /**
     * FIX
     * Catchable Fatal Error: Object of class App\Entity\Allergy could not be converted to string 
     */
    public function __toString()
    {
        return $this->name;
    }

    public function getMultiplier(): ?int
    {
        return $this->multiplier;
    }

    public function setMultiplier(int $multiplier): self
    {
        $this->multiplier = $multiplier;

        return $this;
    }
}