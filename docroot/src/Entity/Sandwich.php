<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SandwichRepository")
 */
class Sandwich
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeBread", inversedBy="sandwiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeBread;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TypeSpread", inversedBy="sandwiches")
     */
    private $typeSpreadId;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\OrderLine", mappedBy="sandwich")
     */
    private $orderLines;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\TypeSize", inversedBy="sandwiches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $typeSize;

    /**
     * @ORM\Column(type="boolean")
     */
    private $aLaCarte;

    public function __construct()
    {
        $this->typeSpreadId = new ArrayCollection();
        $this->orderLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTypeBread(): ?TypeBread
    {
        return $this->typeBread;
    }

    public function setTypeBread(?TypeBread $typeBread): self
    {
        $this->typeBread = $typeBread;

        return $this;
    }

    /**
     * @return Collection|TypeSpread[]
     */
    public function getTypeSpreadId(): Collection
    {
        return $this->typeSpreadId;
    }

    public function addTypeSpreadId(TypeSpread $typeSpreadId): self
    {
        if (!$this->typeSpreadId->contains($typeSpreadId)) {
            $this->typeSpreadId[] = $typeSpreadId;
        }

        return $this;
    }

    public function removeTypeSpreadId(TypeSpread $typeSpreadId): self
    {
        if ($this->typeSpreadId->contains($typeSpreadId)) {
            $this->typeSpreadId->removeElement($typeSpreadId);
        }

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * FIX
     * Catchable Fatal Error: Object of class App\Entity\Allergy could not be converted to string 
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection|OrderLine[]
     */
    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    public function addOrderLine(OrderLine $orderLine): self
    {
        if (!$this->orderLines->contains($orderLine)) {
            $this->orderLines[] = $orderLine;
            $orderLine->setSandwich($this);
        }

        return $this;
    }

    public function removeOrderLine(OrderLine $orderLine): self
    {
        if ($this->orderLines->contains($orderLine)) {
            $this->orderLines->removeElement($orderLine);
            // set the owning side to null (unless already changed)
            if ($orderLine->getSandwich() === $this) {
                $orderLine->setSandwich(null);
            }
        }

        return $this;
    }

    public function getTypeSize(): ?TypeSize
    {
        return $this->typeSize;
    }

    public function setTypeSize(?TypeSize $typeSize): self
    {
        $this->typeSize = $typeSize;

        return $this;
    }

    public function getALaCarte(): ?bool
    {
        return $this->aLaCarte;
    }

    public function setALaCarte(bool $aLaCarte): self
    {
        $this->aLaCarte = $aLaCarte;

        return $this;
    }
}