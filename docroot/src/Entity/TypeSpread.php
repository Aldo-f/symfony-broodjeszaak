<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TypeSpreadRepository")
 */
class TypeSpread
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $calories;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Allergy", inversedBy="typeSpreads")
     */
    private $allergyId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Sandwich", mappedBy="typeSpreadId")
     */
    private $sandwiches;

    public function __construct()
    {
        $this->allergyId = new ArrayCollection();
        $this->sandwiches = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCalories(): ?int
    {
        return $this->calories;
    }

    public function setCalories(?int $calories): self
    {
        $this->calories = $calories;

        return $this;
    }

    /**
     * @return Collection|Allergy[]
     */
    public function getAllergyId(): Collection
    {
        return $this->allergyId;
    }

    public function addAllergyId(Allergy $allergyId): self
    {
        if (!$this->allergyId->contains($allergyId)) {
            $this->allergyId[] = $allergyId;
        }

        return $this;
    }

    public function removeAllergyId(Allergy $allergyId): self
    {
        if ($this->allergyId->contains($allergyId)) {
            $this->allergyId->removeElement($allergyId);
        }

        return $this;
    }

    /**
     * @return Collection|Sandwich[]
     */
    public function getSandwiches(): Collection
    {
        return $this->sandwiches;
    }

    public function addSandwich(Sandwich $sandwich): self
    {
        if (!$this->sandwiches->contains($sandwich)) {
            $this->sandwiches[] = $sandwich;
            $sandwich->addTypeSpreadId($this);
        }

        return $this;
    }

    public function removeSandwich(Sandwich $sandwich): self
    {
        if ($this->sandwiches->contains($sandwich)) {
            $this->sandwiches->removeElement($sandwich);
            $sandwich->removeTypeSpreadId($this);
        }

        return $this;
    }

    /**
     * FIX
     * Catchable Fatal Error: Object of class App\Entity\Allergy could not be converted to string 
     */
    public function __toString()
    {
        return $this->name;
    }
}