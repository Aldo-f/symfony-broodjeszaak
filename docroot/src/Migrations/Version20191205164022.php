<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191205164022 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE allergy (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `order` (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, order_status_id INT NOT NULL, datetime DATETIME NOT NULL, INDEX IDX_F5299398A76ED395 (user_id), INDEX IDX_F5299398D7707B45 (order_status_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_line (id INT AUTO_INCREMENT NOT NULL, sandwich_id INT NOT NULL, order__id INT NOT NULL, quantity INT NOT NULL, INDEX IDX_9CE58EE14D566043 (sandwich_id), INDEX IDX_9CE58EE1251A8A50 (order__id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE order_status (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sandwich (id INT AUTO_INCREMENT NOT NULL, type_bread_id INT NOT NULL, type_size_id INT NOT NULL, name VARCHAR(255) NOT NULL, description VARCHAR(255) DEFAULT NULL, INDEX IDX_8827086890CB2233 (type_bread_id), INDEX IDX_8827086861ACC7D2 (type_size_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE sandwich_type_spread (sandwich_id INT NOT NULL, type_spread_id INT NOT NULL, INDEX IDX_E54E38F34D566043 (sandwich_id), INDEX IDX_E54E38F39D69465E (type_spread_id), PRIMARY KEY(sandwich_id, type_spread_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_bread (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, calories INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_bread_allergy (type_bread_id INT NOT NULL, allergy_id INT NOT NULL, INDEX IDX_1DB578F490CB2233 (type_bread_id), INDEX IDX_1DB578F4DBFD579D (allergy_id), PRIMARY KEY(type_bread_id, allergy_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_size (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_spread (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, calories INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE type_spread_allergy (type_spread_id INT NOT NULL, allergy_id INT NOT NULL, INDEX IDX_A7015C1F9D69465E (type_spread_id), INDEX IDX_A7015C1FDBFD579D (allergy_id), PRIMARY KEY(type_spread_id, allergy_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F5299398D7707B45 FOREIGN KEY (order_status_id) REFERENCES order_status (id)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE14D566043 FOREIGN KEY (sandwich_id) REFERENCES sandwich (id)');
        $this->addSql('ALTER TABLE order_line ADD CONSTRAINT FK_9CE58EE1251A8A50 FOREIGN KEY (order__id) REFERENCES `order` (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_8827086890CB2233 FOREIGN KEY (type_bread_id) REFERENCES type_bread (id)');
        $this->addSql('ALTER TABLE sandwich ADD CONSTRAINT FK_8827086861ACC7D2 FOREIGN KEY (type_size_id) REFERENCES type_size (id)');
        $this->addSql('ALTER TABLE sandwich_type_spread ADD CONSTRAINT FK_E54E38F34D566043 FOREIGN KEY (sandwich_id) REFERENCES sandwich (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sandwich_type_spread ADD CONSTRAINT FK_E54E38F39D69465E FOREIGN KEY (type_spread_id) REFERENCES type_spread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type_bread_allergy ADD CONSTRAINT FK_1DB578F490CB2233 FOREIGN KEY (type_bread_id) REFERENCES type_bread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type_bread_allergy ADD CONSTRAINT FK_1DB578F4DBFD579D FOREIGN KEY (allergy_id) REFERENCES allergy (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type_spread_allergy ADD CONSTRAINT FK_A7015C1F9D69465E FOREIGN KEY (type_spread_id) REFERENCES type_spread (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE type_spread_allergy ADD CONSTRAINT FK_A7015C1FDBFD579D FOREIGN KEY (allergy_id) REFERENCES allergy (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE type_bread_allergy DROP FOREIGN KEY FK_1DB578F4DBFD579D');
        $this->addSql('ALTER TABLE type_spread_allergy DROP FOREIGN KEY FK_A7015C1FDBFD579D');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE1251A8A50');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398D7707B45');
        $this->addSql('ALTER TABLE order_line DROP FOREIGN KEY FK_9CE58EE14D566043');
        $this->addSql('ALTER TABLE sandwich_type_spread DROP FOREIGN KEY FK_E54E38F34D566043');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_8827086890CB2233');
        $this->addSql('ALTER TABLE type_bread_allergy DROP FOREIGN KEY FK_1DB578F490CB2233');
        $this->addSql('ALTER TABLE sandwich DROP FOREIGN KEY FK_8827086861ACC7D2');
        $this->addSql('ALTER TABLE sandwich_type_spread DROP FOREIGN KEY FK_E54E38F39D69465E');
        $this->addSql('ALTER TABLE type_spread_allergy DROP FOREIGN KEY FK_A7015C1F9D69465E');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F5299398A76ED395');
        $this->addSql('DROP TABLE allergy');
        $this->addSql('DROP TABLE `order`');
        $this->addSql('DROP TABLE order_line');
        $this->addSql('DROP TABLE order_status');
        $this->addSql('DROP TABLE sandwich');
        $this->addSql('DROP TABLE sandwich_type_spread');
        $this->addSql('DROP TABLE type_bread');
        $this->addSql('DROP TABLE type_bread_allergy');
        $this->addSql('DROP TABLE type_size');
        $this->addSql('DROP TABLE type_spread');
        $this->addSql('DROP TABLE type_spread_allergy');
        $this->addSql('DROP TABLE user');
    }
}
