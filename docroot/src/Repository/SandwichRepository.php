<?php

namespace App\Repository;

use App\Entity\Sandwich;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Sandwich|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sandwich|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sandwich[]    findAll()
 * @method Sandwich[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SandwichRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Sandwich::class);
    }

    public function checkIfExist($criteria)
    {
        $cnt = sizeof($criteria['typeSpreadId']);

        // Create base query
        $qb =  $this->createQueryBuilder('s')
            // ->select('s.id') // get a full Sandwich or only id? 
            ->innerJoin('s.typeSpreadId', 'ts')
            ->andWhere('s.typeSize = :typeSize')
            ->andWhere('s.typeBread = :typeBread');

        // update query to match numbers of spreads
        for ($i = 0; $i < $cnt; $i++) {
            $i == 0 ? $qb->andWhere("ts.id = :typeSpread$i ") : $qb->orWhere("ts.id = :typeSpread$i ");
            $qb->setParameter("typeSpread$i", $criteria['typeSpreadId'][$i]);
        }

        $qb->groupBy('s.id')
            ->having('COUNT(ts.id) = :cnt')
            ->setParameter('typeSize', $criteria['typeSize']->getId())
            ->setParameter('typeBread', $criteria['typeBread']->getId())
            ->setParameter('cnt', $cnt);

        return $qb->getQuery()->getOneOrNullResult();
    }

    // /**
    //  * @return Sandwich[] Returns an array of Sandwich objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Sandwich
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
