<?php

namespace App\Repository;

use App\Entity\TypeSize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeSize|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeSize|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeSize[]    findAll()
 * @method TypeSize[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeSizeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeSize::class);
    }

    // /**
    //  * @return TypeSize[] Returns an array of TypeSize objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeSize
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
