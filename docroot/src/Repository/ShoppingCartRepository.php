<?php

namespace App\Repository;

use App\Entity\OrderLine;
use App\Entity\Sandwich;
use App\Entity\User;
use App\Entity\ShoppingCart;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method ShoppingCart|null find($id, $lockMode = null, $lockVersion = null)
 * @method ShoppingCart|null findOneBy(array $criteria, array $orderBy = null)
 * @method ShoppingCart[]    findAll()
 * @method ShoppingCart[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShoppingCartRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ShoppingCart::class);
    }


    /**
     * Find the full Cart by User
     *
     * @param ShoppingCart $shoppingCart
     * @return void
     */
    public function findFullCartByUser($values)
    {
        // $QUERY = 'QUERY_BUILDER';
        // $QUERY = 'SQL';
        // $QUERY = "NATIVE_SQL";
        $QUERY = "NATIVE_SQL_V2";

        if ($QUERY == 'QUERY_BUILDER') {
            /** QUERY BUILDER */
            $result =  $this->createQueryBuilder('o')
                ->andWhere('o.user = :userId')
                ->setParameter('userId', $values['userId'])

                ->addSelect([])
                ->getQuery()
                ->getOneOrNullResult();

            // returns Doctrine entities
            return $result;
        }

        $sql = 'SELECT 
            sc.id AS shoppingCartId, sc.user_id, 
            ol.id AS orderLineId, ol.sandwich_id, ol.quantity, 
            s.id AS sandwichId, s.a_la_carte, s.name AS sandwichName, s.description, s.type_bread_id,
            tb.id, tb.name, tb.price
            FROM shopping_cart sc
            INNER JOIN order_line ol ON sc.id = ol.shopping_cart_id
            INNER JOIN sandwich s ON ol.sandwich_id = s.id
            INNER JOIN type_bread tb ON s.type_bread_id = tb.id
            WHERE user_id = :userId';

        if ($QUERY == 'SQL') {
            /** SQL */
            $conn = $this->getEntityManager()->getConnection();

            $stmt = $conn->prepare($sql);
            $stmt->execute(['userId' => $values['userId']]);

            // returns an array of arrays (i.e. a raw data set)
            return $stmt->fetchAll();
        }

        if ($QUERY == 'NATIVE_SQL') {
            // /** NativeQuery */
            // $entityManager = $this->getEntityManager();
            // $rsm = new ResultSetMappingBuilder($entityManager);
            $rsm = new ResultSetMapping();
            $rsm->addEntityResult('App\Entity\ShoppingCart', 'sc');
            $rsm->addFieldResult('sc', 'id', 'id');
            $rsm->addFieldResult('sc', 'user', 'user');
            $rsm->addJoinedEntityResult('App\Entity\OrderLine', 'ol', 'sc', 'orderLine');
            $rsm->addFieldResult('ol', 'orderLineId', 'id');
            $rsm->addFieldResult('ol', 'quantity', 'quantity');
            // $rsm->addFieldResult('ol', 'orderId', 'order_'); // should be of type Order
            // $rsm->addFieldResult('ol', 'sandwichId', 'sandwich', 'App\Entity\Sandwich'); // should be of type Sandwich
            // $rsm->addJoinedEntityResult('App\Entity\Sandwich', 's', 'ol', 'sandwich');
            // $rsm->addFieldResult('s', 'name', 'name');
            // $rsm->addFieldResult('s', 'description', 'description');

            $query = $this->_em->createNativeQuery($sql, $rsm);
            $query->setParameter(':userId', $values['userId']);

            return $query->getResult();
        }

        if ($QUERY == 'NATIVE_SQL_V2') {
            $entityManager = $this->getEntityManager();
            $rsm = new ResultSetMappingBuilder($entityManager);
            $rsm->addRootEntityFromClassMetadata('App\Entity\ShoppingCart', 'sc', ['id' => 'shoppingCartId']);
            $rsm->addJoinedEntityFromClassMetadata('App\Entity\OrderLine', 'ol', 'sc', 'orderLine',  ['sandwich_id' => 'sandwichId', 'id' => 'orderLineId']);
            $rsm->addJoinedEntityFromClassMetadata('App\Entity\Sandwich', 's', 'ol', 'sandwich', ['id'=>'sandwichId', 'name' => 'sandwichName']);
            $rsm->addJoinedEntityFromClassMetadata('App\Entity\TypeBread', 'tb', 's', 'typeBread', []);
            $query = $this->_em->createNativeQuery($sql, $rsm);
            $query->setParameter(':userId', $values['userId']);

            return $query->getOneOrNullResult();
        }
    }

    // /**
    //  * @return ShoppingCart[] Returns an array of ShoppingCart objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ShoppingCart
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}