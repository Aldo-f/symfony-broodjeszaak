<?php

namespace App\Repository;

use App\Entity\TypeSpread;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method TypeSpread|null find($id, $lockMode = null, $lockVersion = null)
 * @method TypeSpread|null findOneBy(array $criteria, array $orderBy = null)
 * @method TypeSpread[]    findAll()
 * @method TypeSpread[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TypeSpreadRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TypeSpread::class);
    }

    // /**
    //  * @return TypeSpread[] Returns an array of TypeSpread objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?TypeSpread
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
