<?php

namespace App\Repository;

use App\Entity\Order;
use DateTime;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @return Order[] Returns an array of Order objects
     */
    public function findFullBy($criteria)
    {
        if (!isset($criteria['limit'])) {
            $criteria['limit'] = 4294967295;
        }
        // $criteria['orderStatus'] = 3; // DEBUG
        $sql =
            'SELECT 
            o.id AS orderId, o.datetime,
            ol.sandwich_id, ol.id AS orderLineId, ol.quantity,
            s.name AS sandwichName, s.id AS sandwichId, s.description,
            ts.id AS typeSizeId, ts.name AS typeSizeName
        FROM
            `order` AS o
        INNER JOIN
            order_line ol ON o.id = ol.order__id        
        INNER JOIN
            sandwich s ON s.id = ol.sandwich_id 
        INNER JOIN
            type_size ts ON s.type_size_id = ts.id
        WHERE
            user_id = :userId AND order_status_id = :orderStatus
        ORDER BY 
            o.id DESC
        LIMIT  :limit';

        $entityManager = $this->getEntityManager();
        $rsm = new ResultSetMappingBuilder($entityManager);
        $rsm->addRootEntityFromClassMetadata('App\Entity\Order', 'o', ['id' => 'orderId']);
        $rsm->addJoinedEntityFromClassMetadata('App\Entity\OrderLine', 'ol', 'o', 'orderLine',  ['sandwich_id' => 'sandwichId', 'id' => 'orderLineId']);
        $rsm->addJoinedEntityFromClassMetadata('App\Entity\Sandwich', 's', 'ol', 'sandwich', ['id' => 'sandwichId', 'name' => 'sandwichName']);
        $rsm->addJoinedEntityFromClassMetadata('App\Entity\TypeSize', 'ts', 's', 'typeSize', ['id' => 'typeSizeId', 'name' => 'typeSizeName']);
        // $rsm->addJoinedEntityFromClassMetadata('App\Entity\TypeBread', 'tb', 's', 'typeBread', []);
        $query = $this->_em->createNativeQuery($sql, $rsm);
        $query->setParameter(':userId', $criteria['user']);
        $query->setParameter(':orderStatus', $criteria['orderStatus']);
        $query->setParameter(':limit', $criteria['limit']);
        $result = $query->getResult();

        return $result;
    }

    /**
     * Check if @user has ordered today
     *
     */
    public function hasOrderedToday($criteria)
    {
        return (bool) $this->createQueryBuilder('o')
            ->select('o.id') // get a full order or only id? 
            ->andWhere('o.user = :user')
            ->andWhere('o.datetime BETWEEN :startDate AND :endDate')
            ->setMaxResults(1)
            ->setParameter('user', $criteria['user']->getId())
            ->setParameter('startDate', (new DateTime())->format('Y-m-d'))
            ->setParameter('endDate', (new DateTime('tomorrow'))->format('Y-m-d'))
            ->getQuery()
            ->getOneOrNullResult();
    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}