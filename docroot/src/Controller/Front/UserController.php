<?php

namespace App\Controller\Front;

use App\Entity\User;
use App\Entity\Order;
use App\Entity\OrderStatus;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class UserController extends AbstractController
{
    /**
     * @Route("/user", name="user")
     */
    public function index()
    {
        if (!$this->getUser()) {
            $this->addFlash('danger', 'You need to log in to see your account details :)');
            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        };

        return $this->render(
            'front/user/index.html.twig',
            [
                // 'orderStatuses' => $orderStatuses,
                // 'user' => $this->getUser()
            ]
        );
    }
}