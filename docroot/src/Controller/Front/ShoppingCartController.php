<?php

namespace App\Controller\Front;

use DateTime;
use App\Entity\Sandwich;
use App\Entity\ShoppingCart;
use App\Repository\ShoppingCartRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route(name="shopping_cart_")
 */
class ShoppingCartController extends AbstractController
{
    /**
     * @Route("/cart", name="index")
     */
    public function index(ShoppingCartRepository $shoppingCartRepository, $isComponent = null)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        // $shoppingCart = $shoppingCartRepository->findFullCartByUser(["userId" => $this->getUser()->getId()]);
        $shoppingCart = $shoppingCartRepository->findOneBy(["user" => $this->getUser()->getId()]);

        // Get full price for this cart
        $isComponent ?
            $total = null : $total = ShoppingCartController::getCartPrice($shoppingCart);

        return $this->render('front/shopping_cart/index.html.twig', [
            'cart' => $shoppingCart,
            'isComponent' => $isComponent,
            'total' => $total
        ]);
    }


    /**
     * 
     * @Route("/shopping_cart/new", name="new", methods={"POST"} )
     * @return void
     */
    public function new(Request $request)
    {
        // TODO: What if used not logged in?

        if (!$this->getUser()) {
            // add message 
            $this->addFlash(
                'danger',
                'You need to be logged in to add a sandwich to your cart'
            );
            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        }

        // Find sandwich by Id
        $sandwichId =  array_keys($request->request->get('submit'))[0];
        $sandwich = $this->getDoctrine()->getRepository(Sandwich::class)->findOneBy(['id' => $sandwichId]);

        if (!$sandwich) {
            $this->addFlash(
                'danger',
                'Didn\'t find a sandwich'
            );
            return  $this->redirectToRoute('front_sandwiches');
        }

        $this->forward('App\Controller\Front\ShoppingCartController::newBySandwich', [
            'sandwich'  => $sandwich
        ]);

        return $this->redirectToRoute('sandwich_index');
    }

    public function newBySandwich(Sandwich $sandwich)
    {
        // Get ShoppingCart by User 
        $shoppingCart = $this->getDoctrine()->getRepository(ShoppingCart::class)->findOneBy(['user' => $this->getUser()]);

        if (!$shoppingCart) {
            $shoppingCart = new ShoppingCart();

            // Get and set data to create Cart 
            $shoppingCart->setDatetime(new DateTime);
            $shoppingCart->setUser($this->getUser());

            // TODO: isValid? 

            // Create Order
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($shoppingCart);
            $entityManager->flush();

            $this->addFlash(
                'primary',
                'Created a shopping cart'
            );
        }
        // Forward this created data to the OrderLineController
        $this->forward('App\Controller\Front\OrderLineController::newShoppingCart', [
            'sandwich' => $sandwich,
            'shoppingCart' => $shoppingCart
        ]);

        return $this->redirectToRoute('sandwich_index');
    }

    public function getCartPrice(ShoppingCart $shoppingCart)
    {
        $cartTotal = 0;
        foreach ($shoppingCart->getOrderLine() as $orderline) {
            $orderTotal = 0;

            $orderTotal += $orderline->getSandwich()->getTypeBread()->getPrice();

            foreach ($orderline->getSandwich()->getTypeSpreadId() as $spread) {
                $orderTotal += $spread->getPrice();
            }

            $multiplier = $orderline->getSandwich()->getTypeSize()->getMultiplier();

            $quantity = $orderline->getQuantity();

            $orderTotal = $orderTotal * ($multiplier / 100) * $quantity;
            $cartTotal += $orderTotal;
        }
        return $cartTotal;
    }
}