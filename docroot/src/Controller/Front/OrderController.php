<?php

namespace App\Controller\Front;

use DateTime;
use App\Entity\Order;
use App\Form\OrderType;
use App\Entity\OrderLine;
use App\Entity\OrderStatus;
use App\Entity\ShoppingCart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * @Route("/order", name="order_")
 */
class OrderController extends AbstractController
{
    // To get config values
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    // /**
    //  * @Route("/", name="index", methods={"GET"})
    //  */
    // public function index(OrderRepository $orderRepository): Response
    // {
    //     $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
    //     $orders = $orderRepository->findBy(["user" => $this->getUser()]);

    //     return $this->render('front/order/index.html.twig', [
    //         'orders' => $orders,
    //     ]);
    // }

    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function orderStatuses()
    {
        if (!$this->getUser()) {
            $this->addFlash('danger', 'You need to log in to see your account details :)');
            $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        };

        $orderStatuses = $this->getDoctrine()->getRepository(OrderStatus::class)->findBy(['id' => [2, 3, 4, 5, 6, 7, 8]]);

        foreach ($orderStatuses as $orderStatus) {
            $orders =  $this->getDoctrine()->getRepository(Order::class)->findBy(['user' => $this->getUser(), 'orderStatus' => $orderStatus], null, 4);
            // $orders =  $this->getDoctrine()->getRepository(Order::class)->findFullBy(['user' => $this->getUser(), 'orderStatus' => $orderStatus, 'limit' => 4]);
            foreach ($orders as $order) {
                $orderStatus->addOrder($order);
            }
        }
        return $this->render(
            'front/order/index.html.twig',
            [
                'orderStatuses' => $orderStatuses,
                'user' => $this->getUser()
            ]
        );
    }

    /**
     * @Route("/status/{orderStatus}", name="show_by_status", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function orderStatus(OrderStatus $orderStatus)
    {
        $orders =  $this->getDoctrine()->getRepository(Order::class)->findFullBy(['user' => $this->getUser(), 'orderStatus' => $orderStatus]);
        foreach ($orders as $order) {
            $orderStatus->addOrder($order);
        }

        return $this->render('front/order/status.html.twig', [
            'orderStatus' => $orderStatus,
        ]);
    }

    /**
     * @Route("/new/from_shopping_cart", name="new_from_shopping_cart", methods={"POST"})
     */
    public function newOrderFromShoppingCart(Request $request)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        // Find ShoppingCart
        $shoppingCartId =  array_keys($request->request->get('submit'))[0];
        $shoppingCart = $this->getDoctrine()->getRepository(ShoppingCart::class)->findOneBy(['id' => $shoppingCartId, 'user' => $this->getUser()]);

        if (!$shoppingCart) {
            $this->addFlash(
                'danger',
                'You can\'t order someone elses shopping cart'
            );
            // return $this->redirectToRoute('order_index');
            return  $this->redirectToRoute('sandwich_index');
        }

        // Check time
        if (!OrderController::timeIsValid(new DateTime()))
            return $this->redirect($_SERVER['HTTP_REFERER']);

        // Has already ordered today?
        if ($this->params->get("app.config.order.limit")) {
            if (OrderController::hasOrderedToday())
                return $this->redirect($_SERVER['HTTP_REFERER']);
        }
        /** 
         * Create new order, get orderId,
         * add to current shoppingCart->getOrderLines => order__id = orderId
         */
        $order = new Order();

        // get data to create Order
        $order->setUser($this->getUser());
        $order->setDatetime(new DateTime);
        $order->setPrice(ShoppingCartController::getCartPrice($shoppingCart));
        //  dd($order);
        // $order->setOrderStatus($this->getDoctrine()->getRepository(OrderStatus::class)->findOneBy(['id' => 3])); // Waiting for payment
        $order->setOrderStatus($this->getDoctrine()->getRepository(OrderStatus::class)->findOneBy(['id' => 4])); // In progress

        // Create Order
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($order);
        $entityManager->flush();

        // Set orderId to shoppingCart
        $orderLines =  $this->getDoctrine()->getRepository(OrderLine::class)->findBy(['shoppingCart' => $shoppingCart]);

        foreach ($orderLines as $orderLine) {
            // Set data
            $orderLine->setOrder($order);
            $orderLine->setShoppingCart(null); // remove orderLine from shoppingCart
            // Update data
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($orderLine);
            $entityManager->flush();
        }

        // // Delete ShoppingCart
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($shoppingCart);
        $entityManager->flush();

        $this->addFlash(
            'success',
            'You created a new order: ' . $order->getId()
        );
        return  $this->redirectToRoute('order_index');
    }

    /**
     * @Route("/{id}", name="show", methods={"GET"}, requirements={"id":"\d+"})
     */
    public function show(Order $order): Response
    {
        return $this->render('front/order/show.html.twig', [
            'order' => $order,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="edit", methods={"GET","POST"},  requirements={"id":"\d+"})
     */
    public function edit(Request $request, Order $order): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('order_index');
        }

        return $this->render('order/edit.html.twig', [
            'order' => $order,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="order_delete", methods={"DELETE"}, requirements={"id":"\d+"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, Order $order): Response
    {
        if ($this->isCsrfTokenValid('delete' . $order->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($order);
            $entityManager->flush();
        }

        return $this->redirectToRoute('order_index');
    }

    // Check time
    public function timeIsValid(DateTime $dateTime)
    {
        if ($dateTime < new DateTime($this->params->get("app.config.order.cutoff"))) {
            return true;
        } else {
            $this->addFlash(
                'warning',
                'You need to order before ' . date_format(new DateTime($this->params->get("app.config.order.cutoff")), 'H:i')
            );
        }
    }

    // Check order today
    public function hasOrderedToday()
    {
        $hasOrderedToday = $this->getDoctrine()->getRepository(Order::class)->hasOrderedToday(['user' => $this->getUser()]);
        if ($hasOrderedToday)
            $this->addFlash(
                'warning',
                'You have already ordered today'
            );
        return $hasOrderedToday;
    }
}