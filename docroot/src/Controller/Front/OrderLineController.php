<?php

namespace App\Controller\Front;

use App\Entity\Order;
use App\Entity\Sandwich;
use App\Entity\OrderLine;
use App\Entity\ShoppingCart;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route("/order/line", name="order_line_")
 */
class OrderLineController extends AbstractController
{
    /**
     * @Route(name="new_shopping_cart")
     */
    public function newShoppingCart(Sandwich $sandwich, ShoppingCart $shoppingCart)
    {
        $orderLine = $this->getDoctrine()->getRepository(OrderLine::class)->findOneBy(['shoppingCart' => $shoppingCart, 'sandwich' => $sandwich]);

        OrderLineController::editShoppingCart($sandwich, $orderLine, $shoppingCart);
    }
    /**
     * @Route(name="new_order")
     */
    public function newOrder(Sandwich $sandwich, Order $order): Response
    {
        $orderLine = $this->getDoctrine()->getRepository(OrderLine::class)->findOneBy(['order_' => $order, 'sandwich' => $sandwich]);

        $orderLine = OrderLineController::editOrder($sandwich, $orderLine, $order);

        // Send the OrderLine back to the Order
        return new Response($orderLine);
    }

    /**
     * @Route("/{id}/edit", name="edit", requirements={"id":"\d+"})
     */
    public function edit(Request $request, OrderLine $orderLine)
    {
        $form = $this->createFormBuilder($orderLine)
            ->add('quantity')
            ->add('save', SubmitType::class, ['label' => 'Update quantity'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $this->getDoctrine()->getManager()->flush();
                $message = 'You have updated the quantity to ' . $form->getData()->getQuantity();
                $this->addFlash(
                    'success',
                    $message
                );
            } else {
                $this->addFlash('danger', 'Submitted form was invalid');
            }
            return $this->redirect($_SERVER['HTTP_REFERER']); // return to last uri
        }

        return $this->render('front/order_line/_form.html.twig', [
            'orderLine' => $orderLine,
            'form' => $form->createView(),
        ]);
    }

    public function editOrder(Sandwich $sandwich, OrderLine $orderLine, Order $order)
    {
        $sandwichName = strtolower($sandwich->getName());
        if (!$orderLine) {
            $orderLine = new OrderLine();

            // get data to create Order
            $orderLine->setSandwich($sandwich);
            $orderLine->setOrder($order);
            $orderLine->setQuantity(1);
            $message = 'Created a new orderLine for ' . $sandwichName;
        } else {
            // Update quantity
            $orderLine->setQuantity($orderLine->getQuantity() + 1);
            $message = 'Updated the quantity of ' . $sandwichName;
        }
        $this->addFlash(
            'primary',
            $message
        );

        // Update or Create OrderLine
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($orderLine);
        $entityManager->flush();

        return $orderLine;
    }

    public function editShoppingCart(Sandwich $sandwich, OrderLine $orderLine = null, ShoppingCart $shoppingCart)
    {
        $sandwichName = strtolower($sandwich->getName());
        if (!$orderLine) {
            $orderLine = new OrderLine();

            // Set data to create Order
            $orderLine->setSandwich($sandwich);
            $orderLine->setShoppingCart($shoppingCart);
            $orderLine->setQuantity(1);
            $message = 'Created a new orderLine for ' . $sandwichName;
        } else {
            // Update quantity
            $orderLine->setQuantity($orderLine->getQuantity() + 1);
            $message = 'Updated the quantity of ' . $sandwichName;
        }
        $this->addFlash(
            'success',
            $message
        );

        // Update or Create OrderLine
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($orderLine);
        $entityManager->flush();
    }

    /**
     * @Route("/{id}", name="delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, OrderLine $orderLine): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        if ($this->isCsrfTokenValid('delete' . $orderLine->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($orderLine);
            $entityManager->flush();
        }

        return $this->redirect($_SERVER['HTTP_REFERER']); // return to last uri
    }
}