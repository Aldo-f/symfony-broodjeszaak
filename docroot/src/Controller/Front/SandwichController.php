<?php

namespace App\Controller\Front;

use App\Entity\Order;
use App\Entity\Sandwich;
use App\Entity\OrderLine;
use App\Form\SandwichType;
use App\Entity\ShoppingCart;
use App\Repository\SandwichRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


/**
 * @Route(name="sandwich_")
 */
class SandwichController extends AbstractController
{
    /**
     * @Route("/", name="index", methods={"GET"})
     */
    public function index(SandwichRepository $sandwichRepository, $aLaCarte = true): Response
    {
        $aLaCarte  ? $sandwiches = $sandwichRepository->findBy(['aLaCarte' => true]) :  $sandwiches =  $sandwichRepository->findAll();

        return $this->render('front/sandwich/index.html.twig', [
            'sandwiches' => $sandwiches,
        ]);
    }

    /**
     * @Route(name="show_orders_by_status")
     */
    public function showOrdersBy($orderStatus = 1)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $orders = $this->getDoctrine()->getRepository(Order::class)->findFullBy(['user' => $this->getUser(), 'orderStatus' => 3]);

        return $this->render('front/sandwich/_orders.html.twig', [
            'order' => $orders,
            'orderStatus' => $orderStatus
        ]);
    }

    /**
     * @Route(name="show_shopping_cart")
     */
    public function showShoppingCart(): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        if ($user) $shoppingCart = $this->getDoctrine()->getRepository(ShoppingCart::class)
            ->findOneBy(['user' => $user]);
        if ($shoppingCart) $orderLines = $this->getDoctrine()->getRepository(OrderLine::class)
            ->findBy(['shoppingCart' => $shoppingCart]);

        return $this->render('front/sandwich/_orders.html.twig', [
            'shoppingCart' => $shoppingCart,
            'orderLines' => $orderLines
        ]);
    }

    /**
     * @Route("sandwich/new", name="new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $cart = "cart";

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();

        $sandwich = new Sandwich();
        $sandwich->setName('Custom sandwich');
        $sandwich->setDescription('Custom sandwich');
        $sandwich->setALaCarte(false);

        // $form = $this->createForm(SandwichType::class, $sandwich);
        $form = $this->createFormBuilder($sandwich)
            ->add('typeBread')
            ->add('typeSpreadId')
            ->add('typeSize')
            // ->add('save', SubmitType::class, ['label' => 'Update quantity'])
            ->add($cart, SubmitType::class, ['label' => 'Add to cart'])
            ->getForm();

        // $form->add($cart, SubmitType::class, ['label' => 'Add to cart']);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            // TODO: check if sandwich already exist
            $typeSpreadsIds = [];
            foreach ($form->getData()->getTypeSpreadId()->getValues() as $value) {
                array_push($typeSpreadsIds, $value->getId());
            }
            $sandwichFromDb = $this->getDoctrine()->getRepository(Sandwich::class)
                ->checkIfExist([
                    'typeBread' => $form->getData()->getTypeBread(),
                    'typeSpreadId' => $typeSpreadsIds,
                    'typeSize' => $form->getData()->getTypeSize(),
                ]);

            if ($sandwichFromDb) {
                // it exist, add this sandwich to the cart? 
                $sandwich = $sandwichFromDb;
                $this->addFlash(
                    'success',
                    'We found the same sandwich you created. We are using that one 😋👌'
                );
            } else {
                // create the sandwich
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($sandwich);
                $entityManager->flush();
            }

            if ($form->getClickedButton()->getConfig()->getName() === $cart) {
                $this->forward('App\Controller\Front\ShoppingCartController::newBySandwich', [
                    'sandwich'  => $sandwich
                ]);
            }


            return $this->redirectToRoute('sandwich_index');
        }

        return $this->render('front/sandwich/new.html.twig', [
            'sandwich' => $sandwich,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("sandwich/{id}", name="show", methods={"GET"},  requirements={"id":"\d+"})
     */
    public function show(Sandwich $sandwich): Response
    {
        return $this->render('front/sandwich/show.html.twig', [
            'sandwich' => $sandwich
        ]);
    }

    /**
     * @Route("sandwich/{id}/edit", name="edit", methods={"GET","POST"},  requirements={"id":"\d+"})
     */
    public function edit(Request $request, Sandwich $sandwich): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');

        $form = $this->createForm(SandwichType::class, $sandwich);
        $form->add('save', SubmitType::class, ['label' => 'Update Sandwich']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('sandwich_index');
        }

        return $this->render('front/sandwich/edit.html.twig', [
            'sandwich' => $sandwich,
            'form' => $form->createView(),
        ]);
    }
}