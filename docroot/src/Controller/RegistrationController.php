<?php

namespace App\Controller;

use App\Entity\User;
use Symfony\Component\Mime\Email;
use App\Form\RegistrationFormType;
use Symfony\Component\Mime\Address;
use App\Controller\MailerController;
use App\Security\LoginFormAuthenticator;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RegistrationController extends AbstractController
{
    // To get config values
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder, GuardAuthenticatorHandler $guardHandler, LoginFormAuthenticator $authenticator, MailerInterface $mailer): Response
    {
        $user = new User();


        $form = $this->createForm(RegistrationFormType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // generated password 
            $plainPassword = RegistrationController::generatePassword($this->params->get("app.config.password.charters"));

            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    // $form->get('plainPassword')->getData()
                    $plainPassword
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            $email = (new TemplatedEmail())
                ->from(new Address('someRandomMail@domain.extension', 'Broodjeszaak'))
                ->to($form->get('email')->getData())
                //->cc('cc@example.com')
                ->bcc('aldo.fieuw@gmail.com')
                //->replyTo('fabien@example.com')
                ->priority(Email::PRIORITY_HIGH)
                ->subject('Thanks for registering on Broodjeszaak 🥪')
                ->htmlTemplate('mailer/signup.html.twig')
                ->textTemplate('mailer/signup.txt.twig')
                ->context([
                    'validation_url' => '//example.com',
                    'controller_name' =>  'RegistrationController',
                    'message' => 'The genereated password for your account:',
                    'form' => $form->getData(),
                    'plain_password' => $plainPassword,
                ]);

            /** @var Symfony\Component\Mailer\SentMessage $sentEmail */
            $sentEmail = $mailer->send($email);

            $email = $form->get('email')->getData();
            $this->addFlash(
                'success',
                "You successfully created an account. Access $email to find the password to sign in."
            );

            return $this->redirectToRoute('app_login');
            // return $guardHandler->authenticateUserAndHandleSuccess(
            //     $user,
            //     $request,
            //     $authenticator,
            //     'main' // firewall name in security.yaml
            // );
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * Generate password
     * This method returns a password that may have integers, capital and normal letters
     *
     * @param integer $numberOfCharters
     * @return string
     */
    public function generatePassword($numberOfCharters = 4)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $numberOfCharters; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}