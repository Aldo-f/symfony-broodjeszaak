<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ForgetType;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use App\Controller\RegistrationController;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SecurityController extends AbstractController
{
    // To get config values
    private $params;

    public function __construct(ParameterBagInterface $params)
    {
        $this->params = $params;
    }
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/forget", name="app_forget")
     */
    public function forgetPassword(Request $request, MailerInterface $mailer, UserPasswordEncoderInterface $passwordEncoder)
    {
        $form = $this->createForm(ForgetType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['email' => $email]);

            if ($user) {
                // generate new password
                $plainPassword = RegistrationController::generatePassword($this->params->get("app.config.password.charters"));
                // encode the new plain password
                $user->setPassword($passwordEncoder->encodePassword($user, $plainPassword));
                // update the user
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($user);
                $entityManager->flush();
                // send mail to user
                SecurityController::sendMail($mailer, $form, $plainPassword);
            }
            // create a message if or if not a user was found
            $this->addFlash('primary', "If a user was found for $email; you got a new password");
        }

        return $this->render('security/forget.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function sendMail($mailer, $form, $plainPassword)
    {
        $email = (new TemplatedEmail())
            ->from(new Address('someRandomMail@domain.extension', 'Broodjeszaak'))
            ->to($form->get('email')->getData())
            //->cc('cc@example.com')
            ->bcc('aldo.fieuw@gmail.com')
            //->replyTo('fabien@example.com')
            ->priority(Email::PRIORITY_HIGH)
            ->subject('You requested a new password for Broodjeszaak🥪')
            ->htmlTemplate('mailer/signup.html.twig')
            ->textTemplate('mailer/signup.txt.twig')
            ->context([
                'controller_name' =>  'RegistrationController',
                'message' => 'Your newly generated password:',
                'form' => $form->getData(),
                'plain_password' => $plainPassword,
            ]);

        /** @var Symfony\Component\Mailer\SentMessage $sentEmail */
        $sentEmail = $mailer->send($email);
    }
}