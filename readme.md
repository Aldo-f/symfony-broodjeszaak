# Extra assignment Symfony - Sandwiches

![Find sandwich by criteria](.bin/img/find_custom_sandwich.PNG)

## Walkthrough

### Create project

#### Terminal:

1. Files

```sh
symfony new docroot
cd docroot && symfony serve -d --port 443
```

```sh
composer require symfony/orm-pack
composer require symfony/maker-bundle --dev
composer require symfony/debug-bundle
composer require form validator twig-bundle annotations security
```

2. Database

```php
## Edit .env file:
# DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/symfony_broodjes?serverVersion=5.7
php bin\console doctrine:database:create
php bin\console mak:user
php bin\console mak:auth
php bin\console mak:registration-form
php bin\console mak:entity [--regenerate]
php bin\console mak:crud
php bin\console mak:migration ## only for creation
php bin\console doctrine:migrations:diff ## for update of the database
php bin\console doctrine:migrations:migrate
```

Check database with MySQL Workbench Reverse Engineer

- [model.pdf](.bin/db/model.pdf)
- [model.mwb](.bin/db/model.mwb)

![make crud](<.bin/img/make crud.png>)

3. Frontend

```sh
composer require symfony/webpack-encore-bundle
yarn install
## Uncomment .enableSassLoader() and const $ = require("jquery"); in webpack.config.js
## /assets/css/app.css ➡ /assets/scss/app.scss
## in /assets/js/app.js; add new scss
yarn add sass-loader@^7.0.1 node-sass --dev
yarn add jquery --dev
yarn encore [dev|prod|dev --watch]
## Edit template to add js and css:
## {{ encore_entry_link_tags('app') }} and  {{ encore_entry_script_tags('app') }}

yarn add bootstrap
## import bootstrap in assets/js/app.js
```

##### Useful commands:

```
lando php bin/console about
```

---

### Create DB's:

- Extra_broodjes

| [ ] User     | [x] Sandwich              | [x] TypeBread      | [x] TypeSpread   | [x] TypeSize         |
| ------------ | ------------------------- | ------------------ | ---------------- | -------------------- |
| <u>id</u>    | <u>id</ul>                | <u>id</u>          | <u>id</u>        | <u>id</u>            |
| email        | <u>typeBread</u> (M2O)    | name               | name             | name                 |
| password     | <u>typeSpreadId</u> (M2M) | price (integer)    | price            | sandwiches (O2M)     |
| roles (json) | <u>typeSize</u> (M2O)     | calories (integer) | calories         | multiplier (integer) |
| orders (O2M) | name                      | allergyId (M2M)    | allergyId (M2M)  |                      |
|              | description               | sandwiches (O2M)   | sandwiches (M2M) |                      |
|              | orderLines (O2M)          |                    |                  |                      |
|              | aLaCarte (boolean)        |                    |                  |                      |

| [x] Order         | [x] OrderLine         | [x] OrderStatus   | [x] Allergy | [x] ShoppingCart |
| ----------------- | --------------------- | ----------------- | ----------- | ---------------- |
| <u>id</u>         | <u>id</u>             | <u>id</u>         | <u>id</u>   | <u>id</u>        |
| user (M2O)        | <u>sandwich</u> (M2O) | ~~orders (O2M) ~~ | name        | datetime         |
| datetime          | <u>order\_</u> (M2O)  | name              | description | orderLine (O2M)  |
| orderStatus (M2O) | quantity              |                   |             | user (M2O)       |
| orderLine (O2M)   |                       |                   |             |
| price             |                       |                   |             |

| [ ] SandwichSpread  |
| ------------------- |
| <u>sandwichId</u>   |
| <u>typeSpreadId</u> |

- `Order`: multiple the same sandwich is possible O2M <--> M2O
- `Sandwich`: a sandwich has one `typeBreadId` and one ore more `typeSpreadId`
- `orderStatus`: Must have a status; not all status can be edited by all users
  - example status: basket, wish, waiting for payment, in progress, on hold, completed, cancelled, reimbursed, ...
- `Callories`: count callories based on the size of the `Sandwich`; collories multiplier?
  - If cheese is 100 calories, a half sized sandwich is 0.5 \* 100 calories

| Abbreviation | Description |
| ------------ | ----------- |
| M2O          | ManyToOne   |
| M2M          | ManyToMany  |
| O2M          | OneToMany   |

#### Bronnen:

- [bulma.io](https://bulma.io/documentation)
- [gulpjs.com](https://gulpjs.com/docs/en/getting-started/quick-start)
- [fomantic-ui](https://fomantic-ui.com/introduction/getting-started.html)
- [Simple Gulp tutorial](https://coder-coder.com/gulp-tutorial-beginners/)
- [stackoverflow - manny-to-manny](https://stackoverflow.com/questions/8062465/sql-field-with-multiple-ids-of-other-table)
- [twig - push value to array](https://ourcodeworld.com/articles/read/622/how-to-push-an-item-to-an-array-in-twig-easily)
- [symfony.com](https://symfony.com/doc/current/setup.html)
  - [login](https://symfony.com/doc/current/security/form_login_setup.html)
  - [user & login](https://symfony.com/blog/new-in-makerbundle-1-8-instant-user-login-form-commands)
  - [frontend](https://symfony.com/doc/current/frontend.html)
  - [encore - simple example](https://symfony.com/doc/current/frontend/encore/simple-example.html)
